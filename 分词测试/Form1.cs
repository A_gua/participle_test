﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using IKAnalyzerNet;                                               //引用类
using IKAnalyzerNet.dict;
using Lucene.Net.Analysis;
using Wintellect.PowerCollections;

namespace 分词测试
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string teststring = richTextBox1.Text;                 //获取字符串
       
            IKAnalyzer ika = new IKAnalyzer();
            System.IO.TextReader r = new System.IO.StringReader(teststring);
            TokenStream ts = ika.TokenStream("TestField", r);      //分词
            for (Token t = ts.Next(); t != null; t = ts.Next())
            {
                richTextBox2.Text += t.TermText() + "\r\n";        //显示内容
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string testString = richTextBox1.Text;                  //获取字符串
            string slen = testString.Length.ToString();             //获取字符串长度

            IKAnalyzer ika = new IKAnalyzer();
            System.IO.TextReader r = new System.IO.StringReader(testString);
            TokenStream ts = ika.TokenStream("TestField", r);
            int m = 0;
            long begin = System.DateTime.Now.Ticks;
            for (Token t = ts.Next(); t != null; t = ts.Next())
            {
                m++;                                                 //显示每项分词结果的序列号、起始字符数、结尾字符数
                richTextBox3.Text += m + ")" + (t.StartOffset() + "," + t.EndOffset() + " = " + t.TermText()) + "\r\n";
            }
            int end = (int)((System.DateTime.Now.Ticks - begin) / 10000);
            richTextBox3.Text += ("长度：" + slen + " 耗时： " + (end) + "ms" + " 分词个数：" + m + " 效率(词/秒)：" + ((int)(m * 1.0f / (end) * 1000))) + "\r\n";
        }
    }
}
